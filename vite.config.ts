import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { fileURLToPath, URL } from "url";
import postcssNesting from "postcss-nested";
import postcssSimpleVars from "postcss-simple-vars";
import postcssMixins from "postcss-mixins";

// https://vitejs.dev/config/
export default defineConfig({
  css: {
    postcss: {
      plugins: [
        postcssNesting,
        postcssMixins,
        postcssSimpleVars({ silent: true }),
      ],
    },
  },
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  plugins: [vue()],
});
